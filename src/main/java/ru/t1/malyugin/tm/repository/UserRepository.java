package ru.t1.malyugin.tm.repository;

import org.apache.commons.lang3.StringUtils;
import ru.t1.malyugin.tm.api.repository.IUserRepository;
import ru.t1.malyugin.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User findOneById(final String id) {
        return users.stream().filter(user -> StringUtils.equals(id, user.getId())).findAny().orElse(null);
    }

    @Override
    public User findOneByLogin(final String login) {
        return users.stream().filter(user -> StringUtils.equals(login, user.getLogin())).findAny().orElse(null);
    }

    @Override
    public User findOneByEmail(final String email) {
        return users.stream().filter(user -> StringUtils.equals(email, user.getEmail())).findAny().orElse(null);
    }

    @Override
    public User remove(final User user) {
        users.remove(user);
        return user;
    }

    @Override
    public void clear() {
        users.clear();
    }

    @Override
    public Boolean isLoginExist(final String login) {
        return users.stream().anyMatch(user -> StringUtils.equals(login, user.getLogin()));
    }

    @Override
    public Boolean isEmailExist(final String email) {
        return users.stream().anyMatch(user -> StringUtils.equals(email, user.getEmail()));
    }

}