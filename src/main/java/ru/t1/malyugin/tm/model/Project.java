package ru.t1.malyugin.tm.model;

import ru.t1.malyugin.tm.api.model.IWBS;
import ru.t1.malyugin.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public final class Project implements IWBS {

    private final String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    private Status status = Status.NOT_STARTED;

    private Date created = new Date();

    public Project() {
    }

    public Project(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

    public Project(final String name, final Status status) {
        this.name = name;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(final Status status) {
        this.status = status;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(final Date created) {
        this.created = created;
    }

    @Override
    public String toString() {
        String result = "";
        boolean isDescription = (description != null && !description.trim().isEmpty());

        result += String.format("%s: %s IN STATUS '%s'", id, name, status.getDisplayName());
        result += (isDescription ? " -> " + description : "");
        return result;
    }

}