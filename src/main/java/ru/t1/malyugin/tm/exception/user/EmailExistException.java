package ru.t1.malyugin.tm.exception.user;

public final class EmailExistException extends AbstractUserException {

    public EmailExistException() {
        super("Error! Email already exists...");
    }

}