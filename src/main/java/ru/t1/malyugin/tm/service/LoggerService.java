package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.t1.malyugin.tm.api.service.ILoggerService;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    private static final String CONFIG_FILE_LOCATION = "/logger.properties";

    private static final String COMMANDS = "COMMANDS";

    private static final String COMMANDS_FILE = "./commands_log.xml";

    private static final String ERRORS = "ERRORS";

    private static final String ERRORS_FILE = "./errors_log.xml";

    private static final String MESSAGES = "MESSAGES";

    private static final String MESSAGES_FILE = "./messages_log.xml";

    private static final LogManager LOG_MANAGER = LogManager.getLogManager();

    private static final Logger LOGGER_ROOT = Logger.getLogger("");

    private static final Logger LOGGER_COMMAND = Logger.getLogger(COMMANDS);

    private static final Logger LOGGER_ERROR = Logger.getLogger(ERRORS);

    private static final Logger LOGGER_MESSAGE = Logger.getLogger(MESSAGES);

    private static final ConsoleHandler CONSOLE_HANDLER = getConsoleHandler();

    public static Logger getLoggerCommand() {
        return LOGGER_COMMAND;
    }

    public static Logger getLoggerError() {
        return LOGGER_ERROR;
    }

    public static Logger getLoggerMessage() {
        return LOGGER_MESSAGE;
    }

    static {
        loadConfigFromFile();
        registry(LOGGER_COMMAND, COMMANDS_FILE, false);
        registry(LOGGER_ERROR, ERRORS_FILE, true);
        registry(LOGGER_MESSAGE, MESSAGES_FILE, true);
    }

    private static void loadConfigFromFile() {
        try {
            final Class<?> clazz = LoggerService.class;
            final InputStream inputStream = clazz.getResourceAsStream(CONFIG_FILE_LOCATION);
            LOG_MANAGER.readConfiguration(inputStream);
        } catch (final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    private static ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private static void registry(final Logger logger, final String fileName, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(CONSOLE_HANDLER);
            logger.setUseParentHandlers(false);
            if (!StringUtils.isBlank(fileName)) logger.addHandler(new FileHandler(fileName));
        } catch (final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }


    @Override
    public void info(final String message) {
        if (StringUtils.isBlank(message)) return;
        LOGGER_MESSAGE.info(message);
    }

    @Override
    public void command(final String message) {
        if (StringUtils.isBlank(message)) return;
        LOGGER_COMMAND.info(message);
    }

    @Override
    public void debug(final String message) {
        if (StringUtils.isBlank(message)) return;
        LOGGER_MESSAGE.fine(message);
    }

    @Override
    public void error(final Exception e) {
        if (e == null) return;
        LOGGER_ERROR.log(Level.SEVERE, e.getMessage(), e);
    }

}