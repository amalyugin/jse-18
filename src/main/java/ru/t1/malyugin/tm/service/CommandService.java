package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.t1.malyugin.tm.api.repository.ICommandRepository;
import ru.t1.malyugin.tm.api.service.ICommandService;
import ru.t1.malyugin.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Override
    public AbstractCommand getCommandByArgument(final String argument) {
        if (StringUtils.isBlank(argument)) return null;
        return commandRepository.getCommandByArgument(argument.trim());
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        if (StringUtils.isBlank(name)) return null;
        return commandRepository.getCommandByName(name.trim());
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

}