package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.t1.malyugin.tm.api.repository.IUserRepository;
import ru.t1.malyugin.tm.api.service.IUserService;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.exception.field.EmailEmptyException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.LoginEmptyException;
import ru.t1.malyugin.tm.exception.field.PasswordEmptyException;
import ru.t1.malyugin.tm.exception.user.EmailExistException;
import ru.t1.malyugin.tm.exception.user.LoginExistException;
import ru.t1.malyugin.tm.exception.user.UserNotFoundException;
import ru.t1.malyugin.tm.model.User;
import ru.t1.malyugin.tm.util.HashUtil;

import java.util.List;

public final class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User add(final User user) {
        if (user == null) throw new UserNotFoundException();
        return userRepository.add(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findOneById(final String id) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        return userRepository.findOneById(id.trim());
    }

    @Override
    public User findOneByLogin(final String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        return userRepository.findOneByLogin(login.trim());
    }

    @Override
    public User findOneByEmail(final String email) {
        if (StringUtils.isBlank(email)) throw new EmailEmptyException();
        return userRepository.findOneByEmail(email.trim());
    }

    @Override
    public User remove(final User user) {
        if (user == null) throw new UserNotFoundException();
        return userRepository.remove(user);
    }

    @Override
    public void clear() {
        userRepository.clear();
    }

    @Override
    public Boolean isLoginExist(final String login) {
        if (StringUtils.isBlank(login)) return false;
        return userRepository.isLoginExist(login);
    }

    @Override
    public Boolean isEmailExist(final String email) {
        if (StringUtils.isBlank(email)) return false;
        return userRepository.isEmailExist(email);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        if (StringUtils.isBlank(password)) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmailExist(email)) throw new EmailExistException();
        final User user = new User();
        user.setLogin(login.trim());
        user.setPasswordHash(HashUtil.salt(password.trim()));
        user.setRole(Role.USUAL);
        if (!StringUtils.isBlank(email)) user.setEmail(email.trim());
        return add(user);
    }

    @Override
    public User create(final String login, final String password, final String email, final Role role) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        if (StringUtils.isBlank(password)) throw new PasswordEmptyException();
        if (StringUtils.isBlank(email)) throw new EmailEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmailExist(email)) throw new EmailExistException();
        final User user = create(login, password, email);
        if (role != null) user.setRole(role);
        return user;
    }

    @Override
    public User removeById(final String id) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        final User user = findOneById(id.trim());
        return remove(user);
    }

    @Override
    public User removeByLogin(final String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        final User user = findOneByLogin(login.trim());
        return remove(user);
    }

    @Override
    public User removeByEmail(final String email) {
        if (StringUtils.isBlank(email)) throw new EmailEmptyException();
        final User user = findOneByEmail(email.trim());
        return remove(user);
    }

    @Override
    public User setPassword(final String id, final String password) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        if (StringUtils.isBlank(password)) throw new PasswordEmptyException();
        final User user = findOneById(id.trim());
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(final String id, final String firstName, final String lastName, final String middleName) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        final User user = findOneById(id.trim());
        if (user == null) throw new UserNotFoundException();
        if (!StringUtils.isBlank(firstName)) user.setFirstName(firstName.trim());
        if (!StringUtils.isBlank(lastName)) user.setLastName(lastName.trim());
        if (!StringUtils.isBlank(middleName)) user.setMiddleName(middleName.trim());
        return user;
    }

}