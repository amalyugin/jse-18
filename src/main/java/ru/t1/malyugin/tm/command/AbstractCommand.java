package ru.t1.malyugin.tm.command;

import ru.t1.malyugin.tm.api.model.ICommand;
import ru.t1.malyugin.tm.api.service.IServiceLocator;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        String result = "";
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();

        boolean isName = (name != null && !name.trim().isEmpty());
        boolean isArgument = (argument != null && !argument.trim().isEmpty());
        boolean isDescription = (description != null && !description.trim().isEmpty());

        result += (isName ? name + (isArgument ? ", " : "") : "");
        result += (isArgument ? argument : "");
        result += (isDescription ? " -> " + description : "");

        return result;
    }

}