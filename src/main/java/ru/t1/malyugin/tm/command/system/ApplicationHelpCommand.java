package ru.t1.malyugin.tm.command.system;

import ru.t1.malyugin.tm.api.service.ICommandService;
import ru.t1.malyugin.tm.command.AbstractCommand;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    private static final String NAME = "help";

    private static final String DESCRIPTION = "Show command list";

    private static final String ARGUMENT = "-h";

    private ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        for (AbstractCommand command : getCommandService().getCommands()) System.out.println(command);
    }

}