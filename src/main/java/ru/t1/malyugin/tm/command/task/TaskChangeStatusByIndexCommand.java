package ru.t1.malyugin.tm.command.task;

import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    private static final String NAME = "task-change-status-by-index";

    private static final String DESCRIPTION = "Change task status by index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer taskIndex = TerminalUtil.nextInteger() - 1;
        System.out.print("ENTER STATUS: ");
        System.out.println(Status.renderValuesList());
        final int statusIndex = TerminalUtil.nextInteger();
        final Status status = Status.getStatusByIndex(statusIndex);
        getTaskService().changeTaskStatusByIndex(taskIndex, status);
    }

}