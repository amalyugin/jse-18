package ru.t1.malyugin.tm.command.project;

import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectListShowCommand extends AbstractProjectCommand {

    private static final String NAME = "project-list";

    private static final String DESCRIPTION = "Show project list";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECTS]");
        System.out.print("ENTER SORT: ");
        System.out.println(Sort.renderValuesList());
        final Integer sortIndex = TerminalUtil.nextIntegerSafe();
        final Sort sort = Sort.getSortByIndex(sortIndex);
        final List<Project> projects = getProjectService().findAll(sort);
        renderProjectList(projects);
    }

}