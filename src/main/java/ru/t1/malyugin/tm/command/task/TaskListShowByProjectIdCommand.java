package ru.t1.malyugin.tm.command.task;

import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListShowByProjectIdCommand extends AbstractTaskCommand {

    private static final String NAME = "task-show-by-project-id";

    private static final String DESCRIPTION = "Show task list by project id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASKS BY PROJECT ID]");
        System.out.print("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = getTaskService().findAllByProjectId(projectId);
        renderTaskList(tasks);
    }

}