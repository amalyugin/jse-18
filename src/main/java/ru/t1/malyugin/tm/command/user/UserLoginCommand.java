package ru.t1.malyugin.tm.command.user;

import ru.t1.malyugin.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    private static final String NAME = "user-login";

    private static final String DESCRIPTION = "user login";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.print("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        final String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }

}