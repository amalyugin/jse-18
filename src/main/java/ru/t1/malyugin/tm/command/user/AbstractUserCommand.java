package ru.t1.malyugin.tm.command.user;

import ru.t1.malyugin.tm.api.service.IAuthService;
import ru.t1.malyugin.tm.api.service.IUserService;
import ru.t1.malyugin.tm.command.AbstractCommand;
import ru.t1.malyugin.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    protected IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected void renderProfile(final User user) {
        String result = "";
        final String email = user.getEmail();
        final String firstName = user.getFirstName();
        final String lastName = user.getLastName();
        final String middleName = user.getMiddleName();
        final String id = user.getId();
        final String login = user.getLogin();
        final String role = user.getRole().getDisplayName();
        boolean isEmail = (email != null);
        boolean isFIO = (firstName != null || lastName != null || middleName != null);
        boolean isFirstName = (firstName != null);
        boolean isLastName = (lastName != null);
        boolean isMiddleName = (middleName != null);

        result += ("ID: " + id);
        result += ("\nLOGIN: " + login);
        result += ("\nROLE: " + role);
        result += (isEmail ? "\nEMAIL: " + email : "");
        result += (isFIO ? "\nFIO: "
                + (isFirstName ? firstName : "")
                + (isMiddleName ? " " + middleName : "")
                + (isLastName ? " " + lastName : "") : "");
        System.out.println(result);
    }

}