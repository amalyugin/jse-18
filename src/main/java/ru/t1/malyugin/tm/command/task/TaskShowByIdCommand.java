package ru.t1.malyugin.tm.command.task;

import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    private static final String NAME = "task-show-by-id";

    private static final String DESCRIPTION = "Show task by id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().findOneById(id);
        renderTask(task);
    }

}