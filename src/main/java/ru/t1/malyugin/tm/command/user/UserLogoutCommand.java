package ru.t1.malyugin.tm.command.user;

public final class UserLogoutCommand extends AbstractUserCommand {

    private static final String NAME = "user-logout";

    private static final String DESCRIPTION = "user logout";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

}