package ru.t1.malyugin.tm.command.user;

import ru.t1.malyugin.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    private static final String NAME = "user-change-password";

    private static final String DESCRIPTION = "change user password";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");
        final String userId = getAuthService().getUserId();
        System.out.print("ENTER NEW PASSWORD: ");
        final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

}