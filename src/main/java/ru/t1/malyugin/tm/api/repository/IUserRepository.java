package ru.t1.malyugin.tm.api.repository;

import ru.t1.malyugin.tm.model.User;

import java.util.List;

public interface IUserRepository {

    User add(User user);

    List<User> findAll();

    User findOneById(String id);

    User findOneByLogin(String login);

    User findOneByEmail(String email);

    User remove(User user);

    void clear();

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}