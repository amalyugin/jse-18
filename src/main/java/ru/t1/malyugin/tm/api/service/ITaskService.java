package ru.t1.malyugin.tm.api.service;

import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.model.Task;

import java.util.List;

public interface ITaskService extends ITaskRepository {

    List<Task> findAll(Sort sort);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

}